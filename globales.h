#define dirMODO 0
#define dirINTSENS 1
#define dirHUMMIN 2
#define dirHUMMAX 3
#define dirRANGOT 6
#define dirTSUPMIN 7
#define dirTSUPMAX 8
#define dirTINFMIN 9
#define dirTINFMAX 10
#define dirHUMSUPMIN 13
#define dirHUMSUPMAX 14
#define dirHUMINFMIN 15
#define dirHUMINFMAX 16
#define dirMINSRIEGO 19
#define btn1 2
#define btn2 3
#define DHTPINSUP 4
#define DHTPININF 5
#define DHTTYPE DHT11
//Reles
#define relBomb 6
#define relVentIn 7
#define relVentEx 8
#define relLuz 9 
// Pines MicroSd
#define CS 10   // Cable Select
#define MOSI 11 // Master Out Slave In
#define MISO 12 // Master In Slave Out
#define SCK 13  // System Clock
// A0 -> A3
#define sHum1 0 //humedad A
#define sHum2 1 //humedad B
#define sHum3 2 //humedad C
#define sHum4 3 //humedad D

bool escritura, cursor, alarmaRiego, tempo, lcdON, riegoManual;
byte dia, mes, hora, minutos;
int  humTemp, ano;
byte hum1, hum2, hum3, hum4;
byte humedadMIN, humedadMAX;
byte humSup, humSupMIN, humSupMAX;
byte humInf, humInfMIN, humInfMAX;
byte horaEncLuz, horaApgLuz, minutosRiego;// minutosRiego MAXIMO 5
byte rangoT; // Termperatura umbral para apagar extracción cuando temperatura baje "rangoT" grados, respecto a la máxima
byte tempSup, tempSupMIN, tempSupMAX;//DHT
byte tempInf, tempInfMIN, tempInfMAX;//DHT
byte tempPro, humProm;
byte gota[] = { 0x08, 0x04, 0x06, 0x0F, 0x1D, 0x1F, 0x1F, 0x0E };
byte grado[] = { 0x0C, 0x12, 0x12, 0x0C, 0x00, 0x00, 0x00, 0x00 };
byte luz[] = { 0x04, 0x15, 0x0E, 0x1F, 0x0E, 0x15, 0x04, 0x00 };
byte vent[] = { 0x08, 0x13, 0x1A, 0x04, 0x0B, 0x19, 0x02, 0x00 };
byte luma[] = { 0x06, 0x0C, 0x18, 0x18, 0x18, 0x1C, 0x0E, 0x07 };
byte modo, intSens;
byte estado;
byte corrMins, corrSegs;

// millis()
unsigned long intervalo_loop=1000;
unsigned long intervalo_boton=300;
unsigned long intervalo_sensado=5000;
unsigned long previoMillisLoop=0;
unsigned long previoMillisBoton=0;
unsigned long previoMillisSens=0;
unsigned long actualMillis;
