# Automatización de Invernadero

Control de invernadero, se encarga de monitorizar la temperatura  
y humedad ambiental en dos niveles, la humedad de 4 macetas.  

Decide cuando cuando debe ingresar aire fresco y/o expulsar aire según  
parametros de control. Y control de ventilación interna.  

Conrola la iluminación para distintas fases de desarrollo, 4 Modos de iluminación.  

Riego automático según humedad en macetas y/o manual.  

Registro en tarjeta SD.  

Información en pantalla de 20 caracteres y 4 lineas.  

Botones **`Boton 1`** y **`Boton 2`** operan según la pantalla(estado) actual.  

### Hardware utilizado:
- [ATmega328p](https://en.wikipedia.org/wiki/Arduino_Nano) (ArduinoNano old bootloader)  
- [Real time clock](https://en.wikipedia.org/wiki/Real-time_clock) (RTC-DS3231)
- [SD card](https://www.arduino.cc/en/reference/SD)
- 2 x [DHT-11](https://playground.arduino.cc/Main/DHT11Lib/)
- [LCD 20x4](http://wiki.sunfounder.cc/index.php?title=I2C_LCD2004)
- 4 x [Sensor de humedad](https://www.geeetech.com/wiki/index.php/Moisture_sensor)
- 2 x [Push Boton](https://www.arduino.cc/en/Tutorial/BuiltInExamples/Button)
- [buck converter](https://en.wikipedia.org/wiki/Buck_converter)

## Estados de operación

### Pantalla principal

Modo principal, control automatizado de invernadero. Muestra información de  
sensores, hora, modo de iluminación, y estado de ventiladores, bomba de riego.  

**`Boton 1`** - Enciende/Apaga riego manual.  
**`Boton 2`** - Cambia pantalla(estado de operación).  
**`Boton1`**+**`Boton2`** - Enciendo o apaga LCD
  
|  |  |  |  |  |  |  |  |  |  |  |  |  |  |   |  |  |  |  |  |
| - | - | - | - | - | - | - | - | - | - | - | - | - | - | -  | - | - | - | - | - |
| `T` | `:` | `2` | `3` | `,` | `5` | `°` | `C` | ` ` | `H` | `:` | `2` | `9` | `%` | `*` | `0` | `3` | `:` | `1` | `4` |
| `T` | `:` | `2` | `2` | `,` | `9` | `°` | `C` | ` ` | `H` | `:` | `3` | `2` | `%` | `*` | `☀` | `M` | `O` | `D` | `☽` |
| `T` | `:` | `2` | `2` | `,` | `3` | `°` | `C` | ` ` | `H` | `:` | `3` | `5` | `%` | `*` | `2` | `0` | `-` | `0` | `4` |
| `A` | `:` | `3` | `5` | `💧️` |`B` | `:` | `3` | `7` | `💧️` | `C` | `:` | `3` | `5` | `💧️` | `D` | `:` | `3` | `2` | `💧️` |

> `☀` : luz encendida  
> `☽` : luz apagada  
> `*` : si está(n) presente(s) indica funcionamiento de ventilador(es) (3)  
> `💧️`: riego encendido  

### Pantalla de **configuración Hora y Fecha**  

Modificar dia, mes, año, hora y minutos del **RTC**

### Pantalla de **configuración Modo y Riego**  

Selección de modo de iluminación y minutos de riego automático.  

### Pantalla de **configuración avanzada**  
Modifica los parametros de control (numerados).  

Variables de control
| Número | Variable | Detalle | Valor por defecto |
| -: | - | - | :-: |
|`1`|**humedadMIN**| Porcentaje de humedad minimo en maceta| 15 |
|`2`|**humedadMAX**| Porcentaje de humedad maximo en maceta | 70 |
|`3`|**RangoT**    | Rango de temperatura en el que deja<br>de actuar el extractor.<br>*rangoT* grados menos que la T° maxima | 2 |
|`4`|**tempSupMIN**| Temperatura Superior Mínima (Extracción) | 20 |
|`5`|**tempSupMAX**| Temperatura Superior Máxima (Extracción) | 26 |
|`6`|**tempInfMIN**| Temperatura Inferior Mínima (Extracción) | 22 |
|`7`|**tempInfMAX**| Temperatura Inferior Mínima (Extracción) | 28 |
|`8`|**humSupMIN** | Humedad Superior Mínima  (Intracción) | 30 |
|`9`|**humSupMAX** | Humedad Superior Máxima  (Intracción) | 50 |
|`10`|**humInfMIN**| Humedad Inferior Mínima  (Intracción) | 40 |
|`11`|**humInfMAX**| Humedad Inferior Máxima  (Intracción) | 60 |

Detalle de [pantallas](https://gitea.kickto.net/SyDeVoS/BabylonGardener/wiki/Manual#pantallas)

