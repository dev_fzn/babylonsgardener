//MANEJAR EXCEPCIONES de sensado.
#include <EEPROM.h>
#include <DHT.h>
#include <RTClib.h>
#include <SPI.h>
#include <SD.h>
#include <LiquidCrystal_I2C.h>
#include "globales.h"
//#include <SoftwareSerial.h>
//SoftwareSerial esp(0,1);
DHT dhtSup(DHTPINSUP, DHTTYPE);
DHT dhtInf(DHTPININF, DHTTYPE);
LiquidCrystal_I2C lcd(0x3F, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE); //I2C address [0x27 o 0x3F]
DateTime fecha;
RTC_DS3231 reloj;

void setup() {
  corrMins=59; // minutos de retraso + 55 (siempre que el retraso sea menor que 5 minutos)
  corrSegs=38; // segundos de retraso
  estado = 1;//escritura=1;
  pinMode(btn1, INPUT);
  pinMode(btn2, INPUT);
  pinMode(relLuz, OUTPUT);
  pinMode(relVentEx, OUTPUT);
  pinMode(relVentIn, OUTPUT);
  pinMode(relBomb, OUTPUT);
  digitalWrite(relLuz,1);// 1 = releOff / 0 = releON
  digitalWrite(relVentEx,1);
  digitalWrite(relVentIn,1);
  digitalWrite(relBomb,1);
  riegoManual=false;
  lcdON = 1;
  varControl();//Carga valores a variables de control
  lcd.begin(20,4);
  lcd.createChar(1, grado);
  lcd.createChar(2, gota);
  lcd.createChar(3, luz);
  lcd.createChar(4, vent);
  lcd.createChar(5, luma);
  dhtSup.begin();
  dhtInf.begin();
  if(!reloj.begin()) {
    lcd.setCursor(1,5);
    lcd.print(F("RTC no encontrado"));
    //lcd.setCursor(2,4);
    //lcd.print(F("no encontrado"));
    //return; // para cancelar la ejecucion
  }
  if(!SD.begin(CS)) {
     lcd.clear();
     lcd.setCursor(1,2);
     lcd.print(F("Tarjeta MicroSD"));
     lcd.setCursor(2,4);
     lcd.print(F("no encontrada"));
     //return; // para cancelar la ejecucion
  }
  File archivo = SD.open("registro.csv", FILE_WRITE);
  archivo.println(F("Fecha-Hora,Humedad A,Humedad B,Humedad C,Humedad D,Humedad-Superior,Humedad-Inferior,Temperatura-Superior,Temperatura-Inferior"));
  archivo.close();
  lcd.clear();
  //Ajustar fecha y hora segun hora de compliacion
  //reloj.adjust(DateTime(__DATE__, __TIME__));
}

void loop() {
  actualMillis = millis();
  switch (estado) {
    case 1:
      if ((unsigned long)(actualMillis - previoMillisLoop) >= intervalo_loop) {
        fecha = reloj.now();
        tomaDatos();                //MANEJAR EXCEPCIONES//
        horaLCD();
        selecMOD();
        releLuz();
        releVentEx();               // Rele:Extractor(N.A.)+VentiladorInterno(N.C.) <--segun Temperatura DHT
        releVentIn();               // Rele:Intractor(N.A.) <-- segun Humedad DHT;
        releBomba();
        guardarSD();                // guarda los datos de sensores en microSD 
        correcHora();               // Correccion horaria por desface RTC
        previoMillisLoop = millis();// guarda tiempo "actual"
      }
      leerBotones();
      break;
    case 2:
      if ((unsigned long)(actualMillis - previoMillisLoop) >= intervalo_loop) {
        fecha = reloj.now();
        lcd.clear();
        lcd.print(F("Modificar fecha/hora"));
        lcd.setCursor(0,1);
        lcd.print(F("boton 1 para aceptar"));
        lcd.setCursor(0,2);
        lcd.print(F("boton 2 para salir"));
        lcd.setCursor(0,3);
        lcd.print(fecha.day());
        lcd.print('/');
        lcd.print(fecha.month());
        lcd.print('/');
        lcd.print(fecha.year());
        lcd.setCursor(11,3);
        if(fecha.hour()>6&&fecha.hour()<20) {
          lcd.write(3);
        } else {
          lcd.write(5);
        }
        lcd.setCursor(12,3);
        lcd.print(getHora('h'));
        lcd.print(':');
        lcd.print(getHora('m'));
        if(cursor) {
          lcd.print(':');
        } else {
          lcd.print(" ");
        }
        lcd.print(getHora('s'));
        cursor=!cursor;
        previoMillisLoop = millis();
      }
      leerBotones();
      break;
    case 3:
      if ((unsigned long)(actualMillis - previoMillisLoop) >= intervalo_loop) {
        lcd.clear();
        lcd.print(F("Modificar Modo/Riego"));
        lcd.setCursor(0,1);
        lcd.print(F("boton 1 para aceptar"));
        lcd.setCursor(0,2);
        lcd.print(F("boton 2 para salir"));
        lcd.setCursor(0,3);
        lcd.print(F("MODO"));
        impModo();
        lcd.setCursor(13,3);
        lcd.write(2);
        lcd.print(':');
        lcd.print(minutosRiego);
        lcd.print(F("mins"));
        previoMillisLoop = millis();
      }
      leerBotones();
      break;
    case 4:
      if ((unsigned long)(actualMillis - previoMillisLoop) >= intervalo_loop) {
        lcd.clear();
        lcd.print(F("++Config. Avanzada++"));
        lcd.setCursor(0,1);
        lcd.print(F("boton 1 para entrar"));
        lcd.setCursor(0,2);
        lcd.print(F("boton 2 para salir"));
        lcd.setCursor(0,3);
        lcd.print(F("11VariablesDeControl"));
        previoMillisLoop = millis();
      }
      leerBotones();
      break;
    default:
      estado=1;
      break;
   }
}

void leerBotones() {
  if( (unsigned long)(actualMillis - previoMillisBoton) >= intervalo_boton ) {
    if(digitalRead(btn1)&&digitalRead(btn2)) {
      if(lcdON) {
        lcd.noBacklight();
        lcdON=0;
        previoMillisBoton=millis();
      } else {
        lcd.backlight();
        lcdON=1;
        previoMillisBoton=millis();
      }
    } else {
      if(digitalRead(btn1)) {
        switch(estado) {
          case 1:
            previoMillisBoton=millis();
            riegoManual = !riegoManual;
            break;
          case 2:
            previoMillisBoton=millis();
            configFecha();
            break;
          case 3:
            previoMillisBoton=millis();
            config3();
            break;
          case 4:
            previoMillisBoton=millis();
            config4();
            break;
        }
      }
      // Mejorar control de estado=estado+1(bug al volver a modificar las variables en cualquier estado, limpia LCD y espera pulsacion)
      if(digitalRead(btn2)) {
        estado+=1;
        if(estado>4) {
          estado=1;
          lcd.clear();
        }
        previoMillisBoton=millis();
      }
    }
  }
}

//Funcion para usar el Boton2 como ENTER
bool boton2enter() {
  actualMillis=millis();
  if( (unsigned long)(actualMillis - previoMillisBoton) >= intervalo_loop ) {
    if(digitalRead(btn2)) {
      previoMillisBoton=millis();
      return 1;
    }
    return 0;
  } else {
    return 0;
  }
}

//desde EEPROM
void varControl() {
  //Ajustar segun caudal y tiempo de respuesta sensores humedad en macetas.
  minutosRiego = EEPROM.read(dirMINSRIEGO);   //EEPROM.put(dirMINSRIEGO, minutosRiego);
  modo = EEPROM.read(dirMODO);                //EEPROM.put(dirMODO, modo);
  intSens = EEPROM.read(dirINTSENS);          //EEPROM.put(dirINTSENS, intSens);
  humedadMIN = EEPROM.read(dirHUMMIN);        //EEPROM.put(dirHUMMIN, humedadMIN);
  humedadMAX = EEPROM.read(dirHUMMAX);        //EEPROM.put(dirHUMMAX, humedadMAX);
  rangoT = EEPROM.read(dirRANGOT);            //EEPROM.put(dirRANGOT, rangoT);
  tempSupMIN = EEPROM.read(dirTSUPMIN);       //EEPROM.put(dirTSUPMIN, tempSupMIN);
  tempSupMAX = EEPROM.read(dirTSUPMAX);       //EEPROM.put(dirTSUPMAX, tempSupMAX);
  tempInfMIN = EEPROM.read(dirTINFMIN);       //EEPROM.put(dirTINFMIN, tempInfMIN)
  tempInfMAX = EEPROM.read(dirTINFMAX);       //EEPROM.put(dirTINFMAX, tempInfMAX);
  humSupMIN = EEPROM.read(dirHUMSUPMIN);      //EEPROM.put(dirHUMSUPMIN, humSupMIN);
  humSupMAX = EEPROM.read(dirHUMSUPMAX);      //EEPROM.put(dirHUMSUPMAX, humSupMAX);
  humInfMIN = EEPROM.read(dirHUMINFMIN);      //EEPROM.put(dirHUMINFMIN, humInfMIN);
  humInfMAX = EEPROM.read(dirHUMINFMAX);      //EEPROM.put(dirHUMINFMAX, humInfMAX);
}

//Sensar Humedad * REVISAR CAST
byte sensarHumedad(byte sensor) {
  humTemp=0;
  for(byte x=0;x<10;x++){
    humTemp += map(analogRead(sensor), 0, 1023, 0, 99);
  }
  humTemp=(humTemp/10);
  return (byte)humTemp;//Probar si es necesario el 'Cast'
}

void dhtLCD() {
  //primera linea
  lcd.setCursor(0, 0);
  lcd.print(F("T:"));
  lcd.print(tempSup);
  lcd.setCursor(6, 0);
  lcd.write(1);
  lcd.print(F("C H:"));
  lcd.print(humSup);
  lcd.print('%');
  //segunda linea
  lcd.setCursor(0, 1);
  lcd.print(F("T:"));
  lcd.print(tempPro);
  lcd.setCursor(6, 1);
  lcd.write(1);
  lcd.print(F("C H:"));
  lcd.print(humProm);
  lcd.print('%');
  //tercera linea
  lcd.setCursor(0, 2);
  lcd.print(F("T:"));
  lcd.print(tempInf);
  lcd.setCursor(6, 2);
  lcd.write(1);
  lcd.print(F("C H:"));
  lcd.print(humInf);
  lcd.print('%');
}

//Muestra datos de los 4 sensores humedad de macetas en LCD
void HumTierraLCD() {
  lcd.setCursor(0, 3);
  lcd.print(F("A:"));
  lcd.print(hum1);
  lcd.setCursor(4, 3);
  lcd.print(F(" B:"));
  lcd.print(hum2);
  lcd.setCursor(9, 3);
  lcd.print(F(" C:"));
  lcd.print(hum3);
  lcd.setCursor(14, 3);
  lcd.print(F(" D:"));
  lcd.print(hum4);
}

void tomaDatos() {
  if ((unsigned long)(actualMillis - previoMillisSens) >= intervalo_sensado) {
    hum1 = sensarHumedad(sHum1);
    hum2 = sensarHumedad(sHum2);
    hum3 = sensarHumedad(sHum3);
    hum4 = sensarHumedad(sHum4);
    datosDHT();
    dhtLCD();
    HumTierraLCD();
    previoMillisSens = millis();
  }
}

//VERIFICAR variables elimin
void config4() {
  //  Pantalla config avanzada var 1-9
  actualMillis=millis();
  lcd.clear();
  lcd.print("1:");
  lcd.print(humedadMIN);
  lcd.setCursor(4,0);
  lcd.print("<-");
  lcd.setCursor(7,0);
  lcd.print("2:");
  lcd.print(humedadMAX);
  lcd.setCursor(14,0);
  lcd.print("3:");
  lcd.print(rangoT);
  lcd.setCursor(0,1);
  lcd.print("4:");
  lcd.print(tempSupMIN);
  lcd.setCursor(7,1);
  lcd.print("5:");
  lcd.print(tempSupMAX);
  lcd.setCursor(14,1);
  lcd.print("6:");
  lcd.print(tempInfMIN);
  lcd.setCursor(0,2);
  lcd.print("7:");
  lcd.print(tempInfMAX);
  lcd.setCursor(7,2);
  lcd.print("8:");
  lcd.print(humSupMIN);
  lcd.setCursor(14,2);
  lcd.print("9:");
  lcd.print(humSupMAX);
  lcd.setCursor(0,3);
  lcd.print(F("boton1:+|boton2:sgte"));
  while(!boton2enter()) {
    actualMillis=millis();
    if( (unsigned long)(actualMillis - previoMillisBoton) >= intervalo_boton ) {
      if(digitalRead(btn1)) {
        humedadMIN += 1;
        if(humedadMIN > 70){
          humedadMIN=10;
        }
        lcd.setCursor(2,0);
        lcd.print(humedadMIN);
        previoMillisBoton=millis();
      }
    }
  }
  lcd.setCursor(4,0);
  lcd.print("  ");
  lcd.setCursor(11,0);
  lcd.print(F("<-"));
  while(!boton2enter()) {
    actualMillis=millis();
    if( (unsigned long)(actualMillis - previoMillisBoton) >= intervalo_boton ) {
      if(digitalRead(btn1)) {
        humedadMAX += 1;
        if(humedadMAX > 99){
          humedadMAX=60;
        }
        lcd.setCursor(9,0);
        lcd.print(humedadMAX);
        previoMillisBoton=millis();
      }
    }
  }
  lcd.setCursor(11,0);
  lcd.print("  ");
  lcd.setCursor(18,0);
  lcd.print(F("<-"));
  while(!boton2enter()) {
    actualMillis=millis();
    if( (unsigned long)(actualMillis - previoMillisBoton) >= intervalo_boton ) {
      if(digitalRead(btn1)) {
        rangoT += 1;
        if(rangoT > 9){
          rangoT=1;
        }
        lcd.setCursor(16,0);
        lcd.print(rangoT);
        previoMillisBoton=millis();
      }
    }
  }
  lcd.setCursor(18,0);
  lcd.print("  ");
  lcd.setCursor(4,1);
  lcd.print("<-");
  while(!boton2enter()) {
    actualMillis=millis();
    if( (unsigned long)(actualMillis - previoMillisBoton) >= intervalo_boton ) {
      if(digitalRead(btn1)) {
        tempSupMIN += 1;
        if(tempSupMIN > 20){
          tempSupMIN=15;
        }
        lcd.setCursor(2,1);
        lcd.print(tempSupMIN);
        previoMillisBoton=millis();
      }
    }
  }
  lcd.setCursor(4,1);
  lcd.print("  ");
  lcd.setCursor(11,1);
  lcd.print(F("<-"));
  while(!boton2enter()) {
    actualMillis=millis();
    if( (unsigned long)(actualMillis - previoMillisBoton) >= intervalo_boton ) {
      if(digitalRead(btn1)) {
        tempSupMAX += 1;
        if( tempSupMAX > 35){
          tempSupMAX=25;
        }
        lcd.setCursor(9,1);
        lcd.print(tempSupMAX);
        previoMillisBoton=millis();
      }
    }
  }
  lcd.setCursor(11,1);
  lcd.print("  ");
  lcd.setCursor(18,1);
  lcd.print(F("<-"));
  while(!boton2enter()) {
    actualMillis=millis();
    if( (unsigned long)(actualMillis - previoMillisBoton) >= intervalo_boton ) {
      if(digitalRead(btn1)) {
        tempInfMIN += 1;
        if(tempInfMIN > 25){
          tempInfMIN=15;
        }
        lcd.setCursor(16,1);
        lcd.print(tempInfMIN);
        previoMillisBoton=millis();
      }
    }
  }
  lcd.setCursor(18,1);
  lcd.print("  ");
  lcd.setCursor(4,2);
  lcd.print(F("<-"));
  while(!boton2enter()) {
    actualMillis=millis();
    if( (unsigned long)(actualMillis - previoMillisBoton) >= intervalo_boton ) {
      if(digitalRead(btn1)) {
        tempInfMAX += 1;
        if(tempInfMAX > 35){
          tempInfMAX=25;
        }
        lcd.setCursor(2,2);
        lcd.print(tempInfMAX);
        previoMillisBoton=millis();
      }
    }
  }
  lcd.setCursor(4,2);
  lcd.print("  ");
  lcd.setCursor(11,2);
  lcd.print(F("<-"));
  while(!boton2enter()) {
    actualMillis=millis();
    if( (unsigned long)(actualMillis - previoMillisBoton) >= intervalo_boton ) {
      if(digitalRead(btn1)) {
        humSupMIN += 1;
        if(humSupMIN > 60){
          humSupMIN=30;
        }
        lcd.setCursor(9,2);
        lcd.print(humSupMIN);
        previoMillisBoton=millis();
      }
    }
  }
  lcd.setCursor(11,2);
  lcd.print("  ");
  lcd.setCursor(18,2);
  lcd.print(F("<-"));
  while(!boton2enter()) {
    actualMillis=millis();
    if( (unsigned long)(actualMillis - previoMillisBoton) >= intervalo_boton ) {
      if(digitalRead(btn1)) {
        humSupMAX += 1;
        if(humSupMAX > 90){
          humSupMAX=60;
        }
        lcd.setCursor(16,2);
        lcd.print(humSupMAX);
        previoMillisBoton=millis();
      }
    }
  }
  //  Pantalla config avanzada var 10-11
  lcd.clear();
  actualMillis=millis();
  lcd.print(F("10:"));
  lcd.print(humInfMIN);
  lcd.setCursor(5,0);
  lcd.print(F("<-"));
  lcd.setCursor(7,0);
  lcd.print(F("11:"));
  lcd.print(humInfMAX);
  lcd.setCursor(0,3);
  lcd.print(F("boton1:+|boton2:sgte"));
  while(!boton2enter()) {
    actualMillis=millis();
    if( (unsigned long)(actualMillis - previoMillisBoton) >= intervalo_boton ) {
      if(digitalRead(btn1)) {
        humInfMIN += 1;
        if(humInfMIN > 60){
          humInfMIN=30;
        }
        lcd.setCursor(3,0);
        lcd.print(humInfMIN);
        previoMillisBoton=millis();
      }
    }
  }
  lcd.setCursor(5,0);
  lcd.print("  ");
  lcd.setCursor(12,0);
  lcd.print(F("<-"));
    while(!boton2enter()) {
    actualMillis=millis();
    if( (unsigned long)(actualMillis - previoMillisBoton) >= intervalo_boton ) {
      if(digitalRead(btn1)) {
        humInfMAX += 1;
        if(humInfMAX > 90){
          humInfMAX=60;
        }
        lcd.setCursor(10,0);
        lcd.print(humInfMAX);
        previoMillisBoton=millis();
      }
    }
  }
  lcd.setCursor(12,0);
  lcd.print("  ");
  // Pantalla Salvar Configuracion
  actualMillis=millis();
  lcd.clear();
  lcd.print(F("+ Guardando Config.+"));
  lcd.setCursor(0,1);
  lcd.print(F("boton 1 modif. datos"));
  lcd.setCursor(0,2);
  lcd.print(F("boton 2 grabar datos"));
  lcd.setCursor(0,3);
  lcd.print(F("Apagar Para Cancelar"));
  while(!boton2enter()) {
    actualMillis=millis();
    if( (unsigned long)(actualMillis - previoMillisBoton) >= intervalo_boton ) {
      if(digitalRead(btn1)) {
        previoMillisBoton=millis();
        config4();
      }
    }
  }
  EEPROM.put(dirHUMMIN, humedadMIN);
  EEPROM.put(dirHUMMAX, humedadMAX);
  EEPROM.put(dirRANGOT, rangoT);
  EEPROM.put(dirTSUPMIN, tempSupMIN);
  EEPROM.put(dirTSUPMAX, tempSupMAX);
  EEPROM.put(dirTINFMIN, tempInfMIN);
  EEPROM.put(dirTINFMAX, tempInfMAX);
  EEPROM.put(dirHUMSUPMIN, humSupMIN);
  EEPROM.put(dirHUMSUPMAX, humSupMAX);
  EEPROM.put(dirHUMINFMIN, humInfMIN);
  EEPROM.put(dirHUMINFMAX, humInfMAX);
  varControl();
  lcd.clear();
}

void config3() {
  actualMillis=millis();
  lcd.clear();
  lcd.print(F("Configurando Modo:"));
  lcd.setCursor(0,1);
  lcd.print(F("boton 1 cambiar modo"));
  lcd.setCursor(0,2);
  lcd.print(F("boton 2 selec. modo"));
  lcd.setCursor(0,3);
  lcd.print(F("--->>>"));
  lcd.setCursor(6,3);
  impModo();
  lcd.setCursor(14,3);
  lcd.print(F("<<<---"));
  while(!boton2enter()) {
    actualMillis=millis();
    if( (unsigned long)(actualMillis - previoMillisBoton) >= intervalo_boton ) {
      if(digitalRead(btn1)) {
        modo += 1;
        if(modo > 4){
          modo=1;
        }
        lcd.setCursor(6,3);
        impModo();
        previoMillisBoton=millis();
      }
    }
  }
  lcd.clear();
  lcd.print(F("Conf. minutos Riego:"));
  lcd.setCursor(0,1);
  lcd.print(F("boton 1 suma un min"));
  lcd.setCursor(0,2);
  lcd.print(F("boton 2 graba mins."));
  lcd.setCursor(0,3);
  lcd.print(F("--->>>"));
  lcd.setCursor(10,3);
  lcd.print(minutosRiego);
  lcd.setCursor(14,3);
  lcd.print(F("<<<---"));
  while(!boton2enter()) {
    actualMillis=millis();
    if( (unsigned long)(actualMillis - previoMillisBoton) >= intervalo_boton ) {
      if(digitalRead(btn1)) {
        minutosRiego += 1;
        if(minutosRiego > 5){
          minutosRiego=1;
          }
        lcd.setCursor(10,3);
        lcd.print(minutosRiego);
        previoMillisBoton=millis();
      }
    }
  }
  lcd.clear();
  lcd.print(F("Guardando Modo/Riego"));
  lcd.setCursor(0,1);
  lcd.print(F("boton 1 modif. datos"));
  lcd.setCursor(0,2);
  lcd.print(F("boton 2 grabar datos"));
  lcd.setCursor(0,3);
  lcd.setCursor(0,3);
  lcd.print(F("MODO"));
  impModo();
  lcd.setCursor(13,3);
  lcd.write(2);
  lcd.print(':');
  lcd.print(minutosRiego);
  lcd.print(F("mins"));
  while(!boton2enter()) {
    actualMillis=millis();
    if( (unsigned long)(actualMillis - previoMillisBoton) >= intervalo_boton ) {
      if(digitalRead(btn1)) {
        previoMillisBoton=millis();
        config3();
      }
    }
  }
  EEPROM.put(dirMODO, modo);
  EEPROM.put(dirMINSRIEGO, minutosRiego);
  varControl();
  lcd.clear();
}

void configFecha() {
  actualMillis=millis();
  //Config Dia
  dia = fecha.day();
  lcd.clear();
  lcd.print(F("Configurando Dia :"));
  lcd.setCursor(0,1);
  lcd.print(F("boton 1 suma un dia"));
  lcd.setCursor(0,2);
  lcd.print(F("boton 2 graba el dia"));
  lcd.setCursor(0,3);
  lcd.print(F("--->>>"));
  lcd.setCursor(9,3);
  lcd.print(dia);
  lcd.setCursor(14,3);
  lcd.print(F("<<<---"));
  while(!boton2enter()) {
    actualMillis=millis();
    if( (unsigned long)(actualMillis - previoMillisBoton) >= intervalo_boton ) {
      if(digitalRead(btn1)) {
        dia += 1;
        if(dia > 31){
          dia=1;
          lcd.setCursor(10,3);
          lcd.print(' ');
          }
        lcd.setCursor(9,3);
        lcd.print(dia);
        previoMillisBoton=millis();
      }
    }
  }
  //Config Mes
  mes = fecha.month();
  lcd.clear();
  lcd.print(F("Configurando Mes :"));
  lcd.setCursor(0,1);
  lcd.print(F("boton 1 suma un mes"));
  lcd.setCursor(0,2);
  lcd.print(F("boton 2 graba el mes"));
  lcd.setCursor(0,3);
  lcd.print(F("--->>>"));
  lcd.setCursor(9,3);
  lcd.print(mes);
  lcd.setCursor(14,3);
  lcd.print(F("<<<---"));
  while(!boton2enter()) {
    actualMillis=millis();
    if( (unsigned long)(actualMillis - previoMillisBoton) >= intervalo_boton ) {
      if(digitalRead(btn1)) {
        mes += 1;
        if(mes > 12){
          mes=1;
          lcd.setCursor(10,3);
          lcd.print(' ');
        }
        lcd.setCursor(9,3);
        lcd.print(mes);
        previoMillisBoton=millis();
      }
    }
  }
  //Config Ano
  ano = fecha.year();
  lcd.clear();
  lcd.print(F("Configurando Ano :"));
  lcd.setCursor(0,1);
  lcd.print(F("boton 1 suma un ano"));
  lcd.setCursor(0,2);
  lcd.print(F("boton 2 graba el ano"));
  lcd.setCursor(0,3);
  lcd.print(F("--->>>"));
  lcd.setCursor(8,3);
  lcd.print(ano);
  lcd.setCursor(14,3);
  lcd.print(F("<<<---"));
  while(!boton2enter()) {
    actualMillis=millis();
    if( (unsigned long)(actualMillis - previoMillisBoton) >= intervalo_boton ) {
      if(digitalRead(btn1)) {
        ano += 1;
        if(ano > 2070){ano=2018;}
        lcd.setCursor(8,3);
        lcd.print(ano);
        previoMillisBoton=millis();
      }
    }
  }
  //Config Hora
  hora = fecha.hour();
  lcd.clear();
  lcd.print(F("Configurando Hora :"));
  lcd.setCursor(0,1);
  lcd.print(F("boton 1 suma una Hr"));
  lcd.setCursor(0,2);
  lcd.print(F("boton 2 graba la Hr"));
  lcd.setCursor(0,3);
  lcd.print(F("--->>>"));
  lcd.setCursor(9,3);
  lcd.print(hora);
  lcd.setCursor(14,3);
  lcd.print(F("<<<---"));
  while(!boton2enter()) {
    actualMillis=millis();
    if( (unsigned long)(actualMillis - previoMillisBoton) >= intervalo_boton ) {
      if(digitalRead(btn1)) {
        hora += 1;
        if(hora > 23){
          hora=0;
          lcd.setCursor(10,3);
          lcd.print(' ');
          }
        lcd.setCursor(9,3);
        lcd.print(hora);
        previoMillisBoton=millis();
      }
    }
  }
  //Config Minutos
  minutos = fecha.minute();
  lcd.clear();
  lcd.print(F("Configurando Minutos"));
  lcd.setCursor(0,1);
  lcd.print(F("boton 1 suma un Min"));
  lcd.setCursor(0,2);
  lcd.print(F("boton 2 graba Minuts"));
  lcd.setCursor(0,3);
  lcd.print(F("--->>>"));
  lcd.setCursor(9,3);
  lcd.print(minutos);
  lcd.setCursor(14,3);
  lcd.print(F("<<<---"));
  while(!boton2enter()) {
    actualMillis=millis();
    if( (unsigned long)(actualMillis - previoMillisBoton) >= intervalo_boton ) {
      if(digitalRead(btn1)) {
        minutos += 1;
        if(minutos > 59){
          minutos=0;
          lcd.setCursor(10,3);
          lcd.print(' ');
          }
        lcd.setCursor(9,3);
        lcd.print(minutos);
        previoMillisBoton=millis();
      }
    }
  }
  //Guardando Fecha/Hora
  lcd.clear();
  lcd.print(F("Guardando Fecha/Hora"));
  lcd.setCursor(0,1);
  lcd.print(F("boton 1 modif. datos"));
  lcd.setCursor(0,2);
  lcd.print(F("boton 2 grabar fecha"));
  lcd.setCursor(0,3);
  lcd.print(dia);
  lcd.print('/');
  lcd.print(mes);
  lcd.print('/');
  lcd.print(ano);
  lcd.print(' ');
  lcd.print(hora);
  lcd.print(':');
  lcd.print(minutos);
  while(!boton2enter()) {
    actualMillis=millis();
    if( (unsigned long)(actualMillis - previoMillisBoton) >= intervalo_boton ) {
      if(digitalRead(btn1)) {
        previoMillisBoton=millis();
        configFecha();
      }
    }
  }
  reloj.adjust(DateTime(ano, mes, dia, hora, minutos, 0));
  lcd.clear();
}

//Control INTRACTOR  Segun humedad
void releVentIn() {
  if( humSup>=humSupMAX || humInf>=humInfMAX) {
    digitalWrite(relVentIn, 0);
    lcd.setCursor(14, 2);
    lcd.write(4);
  } 
  if( ((humSup>=humSupMIN) && (humSup<humSupMAX)) || ((humInf>=humInfMIN) && (humInf<humInfMAX)) || (humSup<=humSupMIN || humInf<=humInfMIN) ) {
      digitalWrite(relVentIn, 1);
      lcd.setCursor(14, 2);
      lcd.print(' ');
  }
}

//Control EXTRACTOR & VENTILADOR INTERNO Segun temperatura.
void releVentEx() {
  if( (tempSup>=tempSupMAX) || (tempInf>=tempInfMAX) ) {
    digitalWrite(relVentEx, 0);
  }
  if ( (tempSup<(tempSupMAX-rangoT)) || (tempInf<(tempInfMAX-rangoT)) ) {
    digitalWrite(relVentEx, 1);
  }
  if(!digitalRead(relVentEx)) {
    lcd.setCursor(14, 0);
    lcd.write(4);
    lcd.setCursor(14, 1);
    lcd.print(' ');
  } else {
    lcd.setCursor(14, 0);
    lcd.print(' ');
    lcd.setCursor(14, 1);
    lcd.write(4);
  }
}

//MODO 1=18/6  2=12/12  3=24/00  4=20/04 Luz/Oscuridad
void selecMOD() {
  switch(modo) {
    case 1:
      horaEncLuz=19;
      horaApgLuz=13;
      lcd.setCursor(16, 1);
      lcd.print("MOD");
      lcd.setCursor(15, 2);
      lcd.print("18-06");
      break;
    case 2:
      horaEncLuz=22;
      horaApgLuz=10;
      lcd.setCursor(16, 1);
      lcd.print("MOD");
      lcd.setCursor(15, 2);
      lcd.print("12-12");
      break;
    case 3:
      horaEncLuz=0;
      horaApgLuz=0;
      lcd.setCursor(16, 1);
      lcd.print("MOD");
      lcd.setCursor(15, 2);
      lcd.print("24-00");
      break;
    case 4:
      horaEncLuz=18;
      horaApgLuz=14;
      lcd.setCursor(16, 1);
      lcd.print("MOD");
      lcd.setCursor(15, 2);
      lcd.print("20-04");
      break;
    default://Modo 4 por defecto
      horaEncLuz=18;
      horaApgLuz=14;
      lcd.setCursor(16, 1);
      lcd.print("MOD");
      lcd.setCursor(15, 2);
      lcd.print("20-04");
  }
}

void impModo(){
  lcd.print(modo);
  if(modo==1){
    lcd.print("(18-06)");
  }
  if(modo==2){
    lcd.print("(12-12)");
  }
  if(modo==3){
    lcd.print("(24-00)");
  }
  if(modo==4){
    lcd.print("(20-04)");
  }
}

//Control LUZ
void releLuz() {
  if(modo != '3') {
    if(fecha.hour() >= horaEncLuz || fecha.hour() < horaApgLuz) {
      digitalWrite(relLuz, 0);
    } else {
      digitalWrite(relLuz, 1);
    }
  } else {
    digitalWrite(relLuz, 0);
  }
  if(!digitalRead(relLuz)) {
    lcd.setCursor(15, 1);
    lcd.write(3);
    lcd.setCursor(19, 1);
    lcd.print(' ');
  } else {
    lcd.setCursor(15, 1);
    lcd.write(' ');
    lcd.setCursor(19, 1);
    lcd.write(5);
  }
}

// riego ocurre 'minutosRiego' minutos cada 10 minutos, calcular valor OPTIMO según caudal de la bomba y respuesta de sensores ||VAR: minutosRiego||
void releBomba() {
  if(!riegoManual) {
    //Riego manual se puede usar en cualquier momento, Riego automatico, solo cuando la luz esta apagagada entre minutos 0 y 'minutosRiego'
    if( fecha.minute()<minutosRiego || (fecha.minute()>=10 && fecha.minute()<(10+minutosRiego)) || (fecha.minute()>=20 && fecha.minute()<(20+minutosRiego)) || (fecha.minute()>=30 && fecha.minute()<(30+minutosRiego)) || (fecha.minute()>=40 && fecha.minute()<(40+minutosRiego)) || (fecha.minute()>=50 && fecha.minute()<(50+minutosRiego)) ) {                                               
      if(modo != 3) {
        //No hay riego si un sensor y promedio exceden valores maximos
        if( digitalRead(relLuz) &&( hum1<=humedadMIN || hum2<=humedadMIN || hum3<=humedadMIN || hum4<=humedadMIN )) {
          alarmaRiego = true;
        }
        if( !digitalRead(relLuz) || hum1>=humedadMAX || hum2>=humedadMAX || hum3>=humedadMAX || hum4>=humedadMAX ) {
          alarmaRiego = false;
        }
      } else {
        if( hum1<=humedadMIN || hum2<=humedadMIN || hum3<=humedadMIN || hum4<=humedadMIN ) {
          alarmaRiego = true;
        }
        //No hay riego si un sensor o promedio exceden valores maximos
        if( hum1>=humedadMAX || hum2>=humedadMAX || hum3>=humedadMAX || hum4>=humedadMAX ) {
          alarmaRiego = false;
        }
      }
    } else {
        alarmaRiego = false;
    }
    if(alarmaRiego) {
      digitalWrite(relBomb, 0);
      lcd.setCursor(4, 3);
      lcd.print(' ');
      lcd.setCursor(9, 3);
      lcd.print(' ');
      lcd.setCursor(14, 3);
      lcd.print(' ');
      lcd.setCursor(19, 3);
      lcd.write(2);
    } else {
      digitalWrite(relBomb, 1);
      lcd.setCursor(4, 3);
      lcd.print(' ');
      lcd.setCursor(9, 3);
      lcd.print(' ');
      lcd.setCursor(14, 3);
      lcd.print(' ');
      lcd.setCursor(19, 3);
      lcd.print(' ');
    }
  } else {
    //Si riego manual esta activado...
    digitalWrite(relBomb, 0);
    lcd.setCursor(4, 3);
    lcd.write(2);
    lcd.setCursor(9, 3);
    lcd.write(2);
    lcd.setCursor(14, 3);
    lcd.write(2);
    lcd.setCursor(19, 3);
    lcd.write(2);
  }
}

//DHT11 Superior e Inferior sensado debe ser > 1 segundo ** if (isnan(humSup)) {Añardir control de mal funcionamiento} !!**
void datosDHT() {
  humSup = dhtSup.readHumidity();     //(byte)dhtSup.readHumidity(); 
  tempSup = dhtSup.readTemperature(); //(byte)dhtSup.readTemperature();
  humInf = dhtInf.readHumidity();     //(byte)dhtInf.readHumidity();
  tempInf = dhtInf.readTemperature(); //(byte)dhtInf.readTemperature();
  humProm = (humSup+humInf)/2;
  tempPro = (tempSup+tempInf)/2;
}

//Añade un 0 a los valores horarios menores a 10
String getHora(char var) {
  switch (var) {
    case 'h':
      if(fecha.hour() < 10) {
        return ("0"+(String)fecha.hour()); break;
        } else {
          return (String)fecha.hour();break;
        }
    case 'm':
      if(fecha.minute() <10) {
        return ("0"+(String)fecha.minute()); break;
        } else {
          return (String)fecha.minute();break;
      }
    case 's':
      if(fecha.second() <10) {
        return ("0"+(String)fecha.second()); break;
        } else {
          return (String)fecha.second();break;
      }
  }
}

//Hora en LCD
void horaLCD() {
  lcd.setCursor(15, 0);
  lcd.print(getHora('h'));
  if(cursor) {
    lcd.print(':');
    cursor=false;
  } else {
    lcd.print(' ');
    cursor=true;    
  }
  lcd.print(getHora('m'));
}

void correcHora() {
  if(fecha.hour() == 3 && fecha.minute() == 55 && fecha.second() == 0) {
    reloj.adjust(DateTime(fecha.year(), fecha.month(), fecha.day(), fecha.hour(), corrMins, corrSegs));
  }
}

void guardarSD() {
  if(fecha.minute()%30 == 0) {
      if(!escritura) {
        String linea = String(fecha.day());
        linea += "/";
        linea += String(fecha.month());
        linea += "/";
        linea += String(fecha.year());
        linea += " ";
        linea += String(fecha.hour());
        linea += ":";
        linea += String(fecha.minute());
        linea += ",";
        linea += String(hum1);
        linea += ",";
        linea += String(hum2);
        linea += ",";
        linea += String(hum3);
        linea += ",";
        linea += String(hum4);
        linea += ",";
        linea += String(humSup);
        linea += ",";
        linea += String(humInf);
        linea += ",";
        linea += String(tempSup);
        linea += ",";
        linea += String(tempInf);
        File archivo = SD.open("registro.csv", FILE_WRITE);
        archivo.println(linea);
        archivo.close();
        escritura=1;
        Serial.println(linea);
      }
    } else {
      escritura=0;
    }
}
